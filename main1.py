import sqlite3
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox
def open_login_page():
    root.deiconify()  # Show the login page

def logout():
    if messagebox.askokcancel("Logout", "Are you sure you want to logout?"):
        open_login_page()
def open_admin_page():
    # Create the main window for the upcoming events
    parent = tk.Toplevel()  # Use Toplevel instead of Tk for additional windows
    parent.title("Upcoming Events")

    def maximize_window():
        parent.state("zoomed")

    parent.after(100, maximize_window)

    # Create a frame to hold the event
    frame = tk.Frame(parent)
    frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    # Define the events
    events = [
        ("NASA HAKATHON", "lightblue"),
        ("ISTE (creative writing)", "pink"),
        ("MOVIE PROMOTIONS", "yellow"),
        ("COORDINATOR SELECTIONS", "orange"),
        ("PLACEMENT DRIVE", "green"),
        ("SPORTS", "purple"),
        ("VPOD SELECTIONS", "violet")
    ]

    # Define a function to display the events
    def display_events(event_list):
        for i, event in enumerate(event_list):
            event_button = tk.Button(frame, text=event[0], bg=get_event_color(i), padx=20, pady=10, bd=0,
                                     font=("Helvetica", 12), wraplength=200, justify="center",
                                     command=lambda e=event: show_event_info(e))
            event_button.grid(row=i, column=0, padx=10, pady=10, sticky="nsew")

    # Define a function to get the color of an event
    def get_event_color(index):
        for event, color in events:
            if events.index((event, color)) == index:
                return color

    # Define a function to display the information of a selected event
    def show_event_info(event):
        event_name = event[0]
        event_info = get_event_info(event_name)

        # Create a new window to display the event information
        event_window = tk.Toplevel(parent)
        event_window.title(event_name)
        event_window.state("zoomed")  # Maximize the window

        # Create a label to display the event information
        event_label = tk.Label(event_window, text=event_info, font=("Helvetica", 12), wraplength=300)
        event_label.pack(padx=10, pady=10)

        # Change the logout button to a back button
        back_button = tk.Button(event_window, text="Back", font=("Arial", 12),
                                command=lambda: event_window.destroy())
        back_button.place(x=10, y=10)

    # Define a function to get the information of an event
    def get_event_info(event_name):
        event_info_dict = {
            "NASA HAKATHON": "NASA HAKATHON is a 36-hour hackathon organized by NASA. It is an opportunity for students to solve real-world problems faced by NASA.",
            "ISTE (creative writing)": "ISTE (creative writing) is a workshop on creative writing organized by ISTE. It is an opportunity for students to learn and improve their creative writing skills.",
            "MOVIE PROMOTIONS": "Movie Promotions are events organized by various movie production houses to promote their upcoming movies. It is an opportunity for students to watch movies before their release and meet the stars.",
            "COORDINATOR SELECTIONS": "Coordinator Selections are events organized by various clubs and societies to select coordinators for the upcoming academic year. It is an opportunity for students to take up leadership roles.",
            "PLACEMENT DRIVE": "Placement Drive is an event organized by the college to provide job opportunities to students. It is an opportunity for students to get placed in their dream companies.",
            "SPORTS": "Sports are events organized by the college to promote physical activity and team spirit. It is an opportunity for students to showcase their sports skills and represent their college.",
            "VPOD SELECTIONS": "VPOD selections for content creator, social media handlers, etc roles are open"
        }
        return event_info_dict.get(event_name, "Event information not available.")

    # Display the events
    display_events(events)
    # Align the events to the center of the frame
    frame.grid_columnconfigure(0, minsize=400)
    frame.grid_rowconfigure(0, minsize=50)

    for i in range(len(events)):
        event_button = frame.grid_slaves(row=i, column=0)[0]
        event_button.grid_configure(sticky="nsew")

    # Add Add Event button
    add_event_button = tk.Button(parent, text="Add Event", font=("Arial", 12), command=add_event)
    add_event_button.pack(anchor='n', padx=10, pady=10)


    # Add Delete Event button
    delete_event_button = tk.Button(parent, text="Delete Event", font=("Arial", 12), command=delete_event)
    delete_event_button.pack(anchor='n', padx=10, pady=10)

    logout_button = tk.Button(parent, text="Logout", font=("Arial", 12), command=logout,bg="red")
    logout_button.pack(side="bottom",padx=10, pady=10)
    # Start the Tkinter event loop for the upcoming events window
    parent.mainloop()
def open_student_page():
    # Create the main window for the upcoming events
    parent = tk.Toplevel()  # Use Toplevel instead of Tk for additional windows
    parent.title("Upcoming Events")

    def maximize_window():
        parent.state("zoomed")

    parent.after(100, maximize_window)

    # Create a frame to hold the event
    frame = tk.Frame(parent)
    frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    # Define the events
    events = [
        ("NASA HAKATHON", "lightblue"),
        ("ISTE (creative writing)", "pink"),
        ("MOVIE PROMOTIONS", "yellow"),
        ("COORDINATOR SELECTIONS", "orange"),
        ("PLACEMENT DRIVE", "green"),
        ("SPORTS", "purple"),
        ("VPOD SELECTIONS", "violet")
    ]

    # Define a function to display the events
    def display_events(event_list):
        for i, event in enumerate(event_list):
            event_button = tk.Button(frame, text=event[0], bg=get_event_color(i), padx=20, pady=10, bd=0,
                                     font=("Helvetica", 12), wraplength=200, justify="center",
                                     command=lambda e=event: show_event_info(e))
            event_button.grid(row=i, column=0, padx=10, pady=10, sticky="nsew")

    # Define a function to get the color of an event
    def get_event_color(index):
        for event, color in events:
            if events.index((event, color)) == index:
                return color

    # Define a function to display the information of a selected event
    def show_event_info(event):
        event_name = event[0]
        event_info = get_event_info(event_name)

        # Create a new window to display the event information
        event_window = tk.Toplevel(parent)
        event_window.title(event_name)
        event_window.state("zoomed")  # Maximize the window

        # Create a label to display the event information
        event_label = tk.Label(event_window, text=event_info, font=("Helvetica", 12), wraplength=300)
        event_label.pack(padx=10, pady=10)

        # Change the logout button to a back button
        back_button = tk.Button(event_window, text="Back", font=("Arial", 12),
                                command=lambda: event_window.destroy())
        back_button.place(x=10, y=10)

    # Define a function to get the information of an event
    def get_event_info(event_name):
        event_info_dict = {
            "NASA HAKATHON": "NASA HAKATHON is a 36-hour hackathon organized by NASA. It is an opportunity for students to solve real-world problems faced by NASA.",
            "ISTE (creative writing)": "ISTE (creative writing) is a workshop on creative writing organized by ISTE. It is an opportunity for students to learn and improve their creative writing skills.",
            "MOVIE PROMOTIONS": "Movie Promotions are events organized by various movie production houses to promote their upcoming movies. It is an opportunity for students to watch movies before their release and meet the stars.",
            "COORDINATOR SELECTIONS": "Coordinator Selections are events organized by various clubs and societies to select coordinators for the upcoming academic year. It is an opportunity for students to take up leadership roles.",
            "PLACEMENT DRIVE": "Placement Drive is an event organized by the college to provide job opportunities to students. It is an opportunity for students to get placed in their dream companies.",
            "SPORTS": "Sports are events organized by the college to promote physical activity and team spirit. It is an opportunity for students to showcase their sports skills and represent their college.",
            "VPOD SELECTIONS": "VPOD selections for content creator, social media handlers, etc roles are open"
        }
        return event_info_dict.get(event_name, "Event information not available.")

    # Display the events
    display_events(events)
    # Align the events to the center of the frame
    frame.grid_columnconfigure(0, minsize=400)
    frame.grid_rowconfigure(0, minsize=50)

    for i in range(len(events)):
        event_button = frame.grid_slaves(row=i, column=0)[0]
        event_button.grid_configure(sticky="nsew")

    logout_button = tk.Button(parent, text="Logout", font=("Arial", 12), command=logout,bg="red")
    logout_button.pack(side="bottom",padx=10, pady=10)
    # Start the Tkinter event loop for the upcoming events window
    parent.mainloop()



def admin_login():
    username = admin_username_entry.get()
    password = admin_password_entry.get()
    if username == "admin" and password == "admin":
        messagebox.showinfo("Admin Login", "Admin logged in successfully!")
        root.withdraw()  # Hide the login page
        open_admin_page()  # Open the next page
    else:
        messagebox.showerror("Admin Login", "Invalid username or password")
def get_person(username ,conn ,cursor):
    cursor.execute("SELECT * FROM login WHERE username=? ",(username,))
    return cursor.fetchone()

def student_login():
    username = student_username_entry.get()
    password = student_password_entry.get()
    t=get_person(username ,conn ,cursor)
    if (t):
        if(t[1]== password):
            messagebox.showinfo("Student signed in")
            open_student_page()
        else:
            messagebox.showinfo("Wrong password")
    else:
        messagebox.showinfo("student not signed in")

    # if username == "student" and password == "student":
    #     messagebox.showinfo("Student Login", "Student logged in successfully!")
    #     root.withdraw()  # Hide the login page
    #     open_student_page()  # Open the next page
    # else:
    #     messagebox.showerror("Student Login", "Invalid username or password")
def add_event():
    add_event_window = tk.Toplevel()
    add_event_window.title("Add Event")
    add_event_window.state("zoomed")

    # Create labels and entry widgets for event details
    labels = ["Event Name:", "Date:", "Venue:", "Time:", "Conducted By:"]
    entries = []

    for i, label_text in enumerate(labels):
        label = tk.Label(add_event_window, text=label_text, font=("Arial", 12))
        label.grid(row=i, column=0, padx=10, pady=10, sticky="e")

        entry = tk.Entry(add_event_window, font=("Arial", 12))
        entry.grid(row=i, column=1, padx=10, pady=10, sticky="w")
        entries.append(entry)

    def go_back():
        add_event_window.destroy()  # Close the current window
        open_admin_page()  # Restore the admin page

    back_button = tk.Button(add_event_window, text="Back", font=("Arial", 12), command=go_back)
    back_button.grid(row=len(labels)+1, columnspan=2, padx=10, pady=10)

    # Function to get the values entered by the user and save to database
    def get_event_details():
        event_details = [entry.get() for entry in entries]  # Retrieve event details from entry widgets
        save_event_to_database(event_details)  # Pass event details to save function
        messagebox.showinfo("Success", "Event details have been added successfully!")
        add_event_window.destroy()  # Close the current window
        open_admin_page()  # Restore the admin page

    # Add a button to submit event details
    submit_button = tk.Button(add_event_window, text="Submit", font=("Arial", 12), command=get_event_details)
    submit_button.grid(row=len(labels), columnspan=2, padx=10, pady=10)
    # save_event_to_database(event_details)

    cursor.execute("INSERT INTO events  EventName=? ,Date=? ,Venue=? ,Time=? ,ConductedBy=?",
                    (event_details[0], event_details[1], event_details[2], event_details[3], event_details[4]))
                   
    conn.commit()

    window_width = add_event_window.winfo_reqwidth()
    window_height = add_event_window.winfo_reqheight()
    position_right = int(add_event_window.winfo_screenwidth() / 2 - window_width / 2)
    position_down = int(add_event_window.winfo_screenheight() / 2 - window_height / 2)
    add_event_window.geometry("+{}+{}".format(position_right, position_down))

def save_event_to_database(event_details):
    # Execute SQL INSERT query to add event details to the database
    cursor.execute("INSERT INTO events (EventName, Date, Venue, Time, ConductedBy) VALUES (?, ?, ?, ?, ?)",
                   (event_details[0], event_details[1], event_details[2], event_details[3], event_details[4]))
    conn.commit()  # Commit the transaction

    # conn.close()

def delete_event():
    def confirm_deletion():
        event_name = event_name_entry.get()
        if event_name:
            confirm = messagebox.askyesno("Confirm Deletion", f"Are you sure you want to delete the event '{event_name}'?")
            if confirm:
                cursor.execute("DELETE FROM events WHERE EventName=?", (event_name,))
                conn.commit()
                messagebox.showinfo("Event Deleted", f"The event '{event_name}' has been deleted successfully!")
                delete_event_window.destroy()  # Close the delete event window
            else:
                delete_event_window.destroy()  # Close the delete event window

    delete_event_window = tk.Toplevel()
    delete_event_window.title("Delete Event")
    delete_event_window.state("zoomed")

    event_name_label = tk.Label(delete_event_window, text="Event Name:", font=("Arial", 12))
    event_name_label.grid(row=0, column=0, padx=10, pady=10, sticky="e")

    event_name_entry = tk.Entry(delete_event_window, font=("Arial", 12))
    event_name_entry.grid(row=0, column=1, padx=10, pady=10, sticky="w")

    delete_button = tk.Button(delete_event_window, text="Delete", font=("Arial", 12), command=confirm_deletion)
    delete_button.grid(row=1, columnspan=2, padx=10, pady=10)

    window_width = delete_event_window.winfo_reqwidth()
    window_height = delete_event_window.winfo_reqheight()
    position_right = int(delete_event_window.winfo_screenwidth() / 2 - window_width / 2)
    position_down = int(delete_event_window.winfo_screenheight() / 2 - window_height / 2)
    delete_event_window.geometry("+{}+{}".format(position_right, position_down))
root = tk.Tk()
root.title("Login Page")
root.configure(background='#f0f0f0')
root.resizable(False, False)
blank_icon = tk.PhotoImage(width=1, height=1)
root.iconphoto(True, blank_icon)
conn=sqlite3.connect("campus.db")
cursor=conn.cursor()


# Get the screen width and height
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

# Define the size and position of frames
frame_width = screen_width // 2
frame_height = screen_height 
frame_padding = 20

# Create container frame
container = tk.Frame(root, bg='#f0f0f0')
container.pack(fill='both', expand=True)

# Create admin frame
admin_frame = tk.Frame(container, bg='#3498db', bd=0)
admin_frame.place(x=0, y=0, width=frame_width, height=frame_height)

# Add admin photo
admin_photo = Image.open("assets/admin_final.ico")
admin_photo = ImageTk.PhotoImage(admin_photo)
admin_photo_label = tk.Label(admin_frame, image=admin_photo, bg='#3498db')
admin_photo_label.image = admin_photo  # Keep a reference to avoid garbage collection
admin_photo_label.pack(pady=(100, 20))

admin_login_label = tk.Label(admin_frame, text="Admin Login", font=("Arial", 16), bg='#3498db', fg='white')
admin_login_label.pack()

admin_login_box = tk.Frame(admin_frame, bg='white')
admin_login_box.pack(padx=100, pady=(20, 100))

admin_username_label = tk.Label(admin_login_box, text="Username:", font=("Arial", 12), bg='white')
admin_username_label.grid(row=0, column=0, sticky='w',padx=(0, 10))

admin_username_entry = tk.Entry(admin_login_box, font=("Arial", 12))
admin_username_entry.grid(row=0, column=1, padx=(10, 0))

admin_password_label = tk.Label(admin_login_box, text="Password:", font=("Arial", 12), bg='white')
admin_password_label.grid(row=1, column=0, sticky='w', padx=(0, 10))

admin_password_entry = tk.Entry(admin_login_box, font=("Arial", 12), show='*')
admin_password_entry.grid(row=1, column=1, padx=(10, 0), pady=(0, 10))

admin_login_button = tk.Button(admin_login_box, text="Login", font=("Arial", 12), bg='#3498db', fg='white', command=admin_login)
admin_login_button.grid(row=2, columnspan=2, pady=(0, 10))

# Create student frame
student_frame = tk.Frame(container, bg='#2ecc71', bd=0)
student_frame.place(x=frame_width, y=0, width=frame_width, height=frame_height)

# Add student photo
student_photo = Image.open("assets/student_final.ico")
student_photo = ImageTk.PhotoImage(student_photo)
student_photo_label = tk.Label(student_frame, image=student_photo, bg='#2ecc71')
student_photo_label.image = student_photo  # Keep a reference to avoid garbage collection
student_photo_label.pack(pady=(100, 20))

student_login_label = tk.Label(student_frame, text="Student Login", font=("Arial", 16), bg='#2ecc71', fg='white')
student_login_label.pack()

student_login_box = tk.Frame(student_frame, bg='white')
student_login_box.pack(padx=100, pady=(20, 100))

student_username_label = tk.Label(student_login_box, text="Username:", font=("Arial", 12), bg='white')
student_username_label.grid(row=0, column=0, sticky='w')

student_username_entry = tk.Entry(student_login_box, font=("Arial", 12))
student_username_entry.grid(row=0, column=1, padx=(10, 0))

student_password_label = tk.Label(student_login_box, text="Password:", font=("Arial", 12), bg='white')
student_password_label.grid(row=1, column=0, sticky='w')

student_password_entry = tk.Entry(student_login_box, font=("Arial", 12), show='*')
student_password_entry.grid(row=1, column=1, padx=(10, 0), pady=(0, 10))

student_login_button = tk.Button(student_login_box, text="Login", font=("Arial", 12), bg='#2ecc71', fg='white', command=student_login)
student_login_button.grid(row=2, columnspan=2, pady=(0, 10))

root.geometry(f"{screen_width}x{screen_height}+0+0")  # Set window size to full screen

# Start the Tkinter event loop
root.mainloop()