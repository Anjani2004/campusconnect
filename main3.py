import sqlite3
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox
from tkinter import PhotoImage

def resize_image(image_path, frame_width, height):
    image = Image.open(image_path)
    image = image.resize((frame_width, height))
    return ImageTk.PhotoImage(image)

def open_login_page():
    root.deiconify()  # Show the login page

def logout():
    if messagebox.askokcancel("Logout", "Are you sure you want to logout?"):
        messagebox.showinfo("Logout","You have been logged out successfully!")
        open_login_page()
def open_admin_page():
    # Create the main window for the upcoming events
    global admin_parent
    admin_parent = tk.Toplevel()  # Use Toplevel instead of Tk for additional windows
    admin_parent.title("Admin Page")
    admin_parent.configure(background="lightblue")
    admin_parent.resizable(False, False)
    welcome_label = tk.Label(admin_parent, text=f"Welcome Admin!", font=("Arial", 14))
    welcome_label.pack(anchor='nw', padx=10, pady=10)
    def maximize_window():
        admin_parent.state("zoomed")

    admin_parent.after(100, maximize_window)

    add_event_button = tk.Button(admin_parent, text="Add Event", font=("Arial", 12), command=add_event)
    add_event_button.pack(anchor='w', padx=10, pady=10)

    # Add Delete Event button
    delete_event_button = tk.Button(admin_parent, text="Delete Event", font=("Arial", 12), command=delete_event)
    delete_event_button.pack(anchor='w', padx=10, pady=10)

    # Create a frame to hold the event
    global frame
    frame = tk.Frame(admin_parent,bg='#ffffcc')
    frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    # Get events from the database
    events = get_events_from_database()

    # Display the events
    display_events(frame, events)
    # Add Add Event button
    frame.grid_columnconfigure(0, minsize=400)
    frame.grid_rowconfigure(0, minsize=50)

    for i in range(len(events)):
        event_button = frame.grid_slaves(row=i, column=0)[0]
        event_button.grid_configure(sticky="nsew")

    logout_button = tk.Button(admin_parent,text="Logout", font=("Arial", 12), command=logout,bg="red")
    logout_button.pack(side='bottom', padx=10, pady=10)
    

    admin_parent.mainloop()
    # Define a function to display the events

# Connect to the SQLite database
def get_events_from_database():
    conn = sqlite3.connect('campus.db')
    cursor = conn.cursor()
    cursor.execute("SELECT EventName FROM events")
    events = cursor.fetchall()
    conn.close()
    return events
def get_event_info(event_name):
    conn = sqlite3.connect('campus.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM events WHERE EventName=?", (event_name,))
    event_details = cursor.fetchone()
    conn.close()

    # Convert the fetched details into a dictionary for easier access
    columns = [description[0] for description in cursor.description]
    event_details_dict = dict(zip(columns, event_details))

    return event_details_dict
# Update display_events function to use database events
def display_events(frame, events):
    for i, event in enumerate(events):
        event_name = event[0]
        event_button = tk.Button(frame, text=event_name, padx=20, pady=10, bd=0,
                                 font=("arial", 12,"bold"), wraplength=200, justify="center",
                                 command=lambda e=event: show_event_info(e), bg='lightyellow')
        event_button.grid(row=i, column=0, padx=10, pady=10, sticky="nsew")

    # Define a function to display the information of a selected event
    def show_event_info(event):
        event_name = event[0]
        event_info = get_event_info(event_name)

    # Create a new window to display the event information
        event_window = tk.Toplevel()
        event_window.title(event_name)
        event_window.state("zoomed")
        event_window.resizable(False,False)
        event_window.configure(background="lightgreen")
          # Maximize the window

    # Get the event details from the database
        event_details = get_event_details_from_database(event_name)

    # Create and display labels for each detail of the event
        row = 0
        for column_name, column_value in event_details.items():
            label = tk.Label(event_window, text=f"{column_name.capitalize()}: {column_value}", font=("Helvetica", 12))
            label.grid(row=row, column=0, padx=10, pady=5, sticky="w")
            row += 1

    # Change the logout button to a back button
        back_button = tk.Button(event_window, text="Back", font=("Arial", 12), command=event_window.destroy)
        back_button.grid(row=row, column=0, padx=10, pady=10)
def get_event_details_from_database(event_name):
    conn = sqlite3.connect('campus.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM events WHERE EventName=?", (event_name,))
    event_details = cursor.fetchone()
    conn.close()

    # Convert the fetched details into a dictionary for easier access
    columns = [description[0] for description in cursor.description]
    event_details_dict = dict(zip(columns, event_details))

    return event_details_dict
    
    # Start the Tkinter event loop for the upcoming events window
def open_student_page():
    # Create the main window for the upcoming events
    parent = tk.Toplevel()  # Use Toplevel instead of Tk for additional windows
    parent.title("Student Page")
    parent.configure(background='light blue')
    student_username = student_username_entry.get()
    parent.resizable(False, False)
    # Display welcome message with username
    welcome_label = tk.Label(parent, text=f"Welcome : {student_username}", font=("Times New Roman", 20, "bold"))
    welcome_label.pack(anchor='nw', padx=10, pady=10)


    def maximize_window():
        parent.state("zoomed")

    parent.after(100, maximize_window)

    # Create a frame to hold the event
    frame = tk.Frame(parent,bg='#ffffcc')
    frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    events = get_events_from_database()

    # Display the events
    display_events(frame, events)
    # Add Add Event button
    frame.grid_columnconfigure(0, minsize=400)
    frame.grid_rowconfigure(0, minsize=50)

    for i in range(len(events)):
        event_button = frame.grid_slaves(row=i, column=0)[0]
        event_button.grid_configure(sticky="nsew")

    logout_button = tk.Button(parent,text="Logout", font=("Arial", 12), command=logout,bg="red")
    logout_button.pack(side="bottom",padx=10, pady=10)
    

    parent.mainloop()

def get_events_from_database():
    conn = sqlite3.connect('campus.db')
    cursor = conn.cursor()
    cursor.execute("SELECT EventName FROM events")
    events = cursor.fetchall()
    conn.close()
    return events
def display_events(frame, events):
    for i, event in enumerate(events):
        event_name = event[0]
        event_button = tk.Button(frame, text=event_name, padx=20, pady=10, bd=0,
                                 font=("Helvetica", 12,"bold"), wraplength=200, justify="center",
                                 command=lambda e=event: show_event_info(e), bg='lightyellow')
        event_button.grid(row=i, column=0, padx=10, pady=10, sticky="nsew")

    # Define a function to display the information of a selected event
    def show_event_info(event):
        event_name = event[0]
        event_info = get_event_info(event_name)

        # Create a new window to display the event information
        event_window = tk.Toplevel()
        event_window.title(event_name)
        event_window.state("zoomed")
        event_window.resizable(False,False)
        event_window.configure(background='light green')  # Set background color to light 

        # Increase font size for event details
        font_size = 16  # Adjust font size as needed

        total_height = len(event_info) * 40  # Increased height assuming 40 pixels for each label's height

        # Calculate the starting y-coordinate to center align
        start_y = (event_window.winfo_screenheight() - total_height) // 2

        # Create and display labels for each detail of the event with increased font size
        for index, (column_name, column_value) in enumerate(event_info.items()):
            label = tk.Label(event_window, text=f"{column_name.capitalize()}: {column_value}", 
                             font=("Helvetica", font_size))
            label.place(relx=0.5, y=start_y + index * 40, anchor="center")

        # Change the logout button to a back button
        back_button = tk.Button(event_window, text="Back", font=("Arial", 12), command=event_window.destroy)
        back_button.place(relx=0.5, rely=0.9, anchor="center")
def get_event_details_from_database(event_name):
    conn = sqlite3.connect('campus.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM events WHERE EventName=?", (event_name,))
    event_details = cursor.fetchone()
    conn.close()

    # Convert the fetched details into a dictionary for easier access
    columns = [description[0] for description in cursor.description]
    event_details_dict = dict(zip(columns, event_details))

    return event_details_dict
def admin_login():
    username = admin_username_entry.get()
    password = admin_password_entry.get()
    if username == "admin" and password == "admin":
        messagebox.showinfo("Admin Login", "Admin logged in successfully!")
        root.withdraw()  # Hide the login page
        open_admin_page()  # Open the next page
    else:
        messagebox.showerror("Admin Login", "Invalid username or password")
def get_person(username ,conn ,cursor):
    cursor.execute("SELECT * FROM login WHERE username=? ",(username,))
    return cursor.fetchone()

def student_login():
    username = student_username_entry.get()
    password = student_password_entry.get()
    t=get_person(username ,conn ,cursor)
    if (t):
        if(t[1]== password):
            messagebox.showinfo("Student login","student logged in successfully!")
            open_student_page()
        else:
            messagebox.showinfo("Student login","Wrong Username or Password")
    else:
        messagebox.showinfo("student not signed in")

def add_event():
    global add_event_window
    add_event_window = tk.Toplevel()
    add_event_window.title("Add Event")
    add_event_window.state("zoomed")
    add_event_window.resizable(False,False)
    add_event_window.configure(bg="lightpink")
    
    # Calculate the center position of the window
    window_width = add_event_window.winfo_screenwidth()
    window_height = add_event_window.winfo_screenheight()
    position_x = window_width // 2
    position_y = window_height // 2

    # Create labels and entry widgets for event details
    labels = ["Event Name:", "Date:", "Venue:", "Time:", "Conducted By:"]
    entries = []

    for i, label_text in enumerate(labels):
        label = tk.Label(add_event_window, text=label_text, font=("Arial", 12))
        label.place(relx=0.3, rely=0.2 + i * 0.1, anchor="center")

        entry = tk.Entry(add_event_window, font=("Arial", 12))
        entry.place(relx=0.5, rely=0.2 + i * 0.1, anchor="center")
        entries.append(entry)

    def go_back():
        add_event_window.destroy()  # Close the current window
        open_admin_page()  # Restore the admin page

    back_button = tk.Button(add_event_window, text="Back", font=("Arial", 12), command=go_back)
    back_button.place(relx=0.5, rely=0.9, anchor="center")

    # Function to get the values entered by the user and save to database
    def get_event_details():
        event_details = [entry.get() for entry in entries]  # Retrieve event details from entry widgets
        save_event_to_database(event_details)  # Pass event details to save function
        messagebox.showinfo("Success", "Event details have been added successfully!")
        go_back()

    # Add a button to submit event details
    submit_button = tk.Button(add_event_window, text="Submit", font=("Arial", 12), command=get_event_details)
    submit_button.place(relx=0.5, rely=0.8, anchor="center")

    # Center the window
    add_event_window.geometry("+{}+{}".format(position_x, position_y))

def save_event_to_database(event_details):
    cursor.execute("INSERT INTO events (EventName, Date, Venue, Time, ConductedBy) VALUES (?, ?, ?, ?, ?)",
                   (event_details[0], event_details[1], event_details[2], event_details[3], event_details[4]))
    conn.commit()


    window_width = add_event_window.winfo_reqwidth()
    window_height = add_event_window.winfo_reqheight()
    position_right = int(add_event_window.winfo_screenwidth() / 2 - window_width / 2)
    position_down = int(add_event_window.winfo_screenheight() / 2 - window_height / 2)
    add_event_window.geometry("+{}+{}".format(position_right, position_down))

def save_event_to_database(event_details):
    # Execute SQL INSERT query to add event details to the database
    cursor.execute("INSERT INTO events (EventName, Date, Venue, Time, ConductedBy) VALUES (?, ?, ?, ?, ?)",
                   (event_details[0], event_details[1], event_details[2], event_details[3], event_details[4]))
    conn.commit()  # Commit the transaction

    # conn.close()

def delete_event():
    def confirm_deletion():
        event_name = event_name_entry.get()
        if event_name:
            confirm = messagebox.askyesno("Confirm Deletion", f"Are you sure you want to delete the event '{event_name}'?")
            if confirm:
                cursor.execute("DELETE FROM events WHERE EventName=?", (event_name,))
                conn.commit()
                messagebox.showinfo("Event Deleted", f"The event '{event_name}' has been deleted successfully!")
                delete_event_window.destroy()
                admin_parent.destroy()
                open_admin_page()
                

                # Close the delete event window
              # Update the events displayed in the main window
            else:
                delete_event_window.destroy()  # Close the delete event window

    def update_events(frame):
        for widget in frame.winfo_children():
            widget.destroy()
        events = get_events_from_database()
        display_events(frame, events)
        open_admin_page()

    def go_back():
        # Close the current window
        delete_event_window.destroy()
        # Update the events displayed in the main window
        update_events(frame)

    global delete_event_window
    delete_event_window = tk.Toplevel()
    delete_event_window.title("Delete Event")
    delete_event_window.state("zoomed")
    delete_event_window.resizable(False,False)
    

    event_name_label = tk.Label(delete_event_window, text="Event Name:", font=("Arial", 12))
    event_name_label.place(relx=0.3, rely=0.4, anchor="center")

    event_name_entry = tk.Entry(delete_event_window, font=("Arial", 12))
    event_name_entry.place(relx=0.5, rely=0.4, anchor="center")

    delete_button = tk.Button(delete_event_window, text="Delete", font=("Arial", 12), command=confirm_deletion)
    delete_button.place(relx=0.5, rely=0.5, anchor="center")

    back_button = tk.Button(delete_event_window, text="Back", font=("Arial", 12), command=delete_event_window.destroy)
    back_button.place(relx=0.5, rely=0.6, anchor="center")

    delete_event_window.configure(bg="lightblue")  # Set background color

    # Center the window on the screen
    window_width = delete_event_window.winfo_reqwidth()
    window_height = delete_event_window.winfo_reqheight()
    position_x = delete_event_window.winfo_screenwidth() // 2 - window_width // 2
    position_y = delete_event_window.winfo_screenheight() // 2 - window_height // 2
    delete_event_window.geometry("+{}+{}".format(position_x, position_y))

root = tk.Tk()
root.title("Login Page")
root.configure(background='lavender')
root.resizable(False, False)
blank_icon = tk.PhotoImage(width=1, height=1)
root.iconphoto(True, blank_icon)
conn=sqlite3.connect("campus.db")
cursor=conn.cursor()
# Get the screen width and height
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

# Define the size and position of frames
frame_width = screen_width // 2
frame_height = screen_height 
frame_padding = 20
background_color = '#ADD8E6'
# Create container frame
container = tk.Frame(root)
container.pack(fill='both', expand=True)

# Create admin frame
admin_frame = tk.Frame(container, bd=0, bg='lavender')
admin_frame.place(x=0, y=0, width=frame_width, height=frame_height)
admin_background_label = tk.Label(admin_frame,bg='lavender')
admin_background_label.place(relwidth=1, relheight=1)
# Add admin photo
# Resize the admin photo
admin_photo = resize_image("assets/admin_final.ico", 175, 175)
admin_photo_label = tk.Label(admin_frame, image=admin_photo, bg='lavender')
admin_photo_label.image = admin_photo
admin_photo_label.pack(pady=(100, 20))

admin_login_label = tk.Label(admin_frame, text="ADMIN LOGIN", font=("Times New Roman", 19,"bold"), bg='lavender')
admin_login_label.pack()

admin_login_box = tk.Frame(admin_frame,bg='lavender')
admin_login_box.pack(padx=100, pady=(20, 100))

admin_username_label = tk.Label(admin_login_box, text="USERNAME:", font=("Times New Roman", 15), bg='lavender')
admin_username_label.grid(row=0, column=0, sticky='w',padx=(0, 10))

admin_username_entry = tk.Entry(admin_login_box, font=("Times New Roman", 15))
admin_username_entry.grid(row=0, column=1, padx=(10, 0))

admin_password_label = tk.Label(admin_login_box, text="PASSWORD:", font=("Times New Roman", 15), bg='lavender')
admin_password_label.grid(row=1, column=0, sticky='w', padx=(0, 10))

admin_password_entry = tk.Entry(admin_login_box, font=("Times New Roman", 15), show='*')
admin_password_entry.grid(row=1, column=1, padx=(10, 0), pady=(0, 10))

admin_login_button = tk.Button(admin_login_box, text="LOGIN", font=("Times New Roman", 15),bg='green', command=admin_login)
admin_login_button.grid(row=2, columnspan=2, pady=(0, 10))

# Create student frame
student_frame = tk.Frame(container, bd=0, bg='lavender')
student_frame.place(x=frame_width, y=0, width=frame_width, height=frame_height)
student_background_label = tk.Label(student_frame,bg='lavender')
student_background_label.place(relwidth=1, relheight=1)
# Add student photo
# Resize the student photo
student_photo = resize_image("assets/student_final.ico", 175, 175)
student_photo_label = tk.Label(student_frame, image=student_photo, bg='lavender')
student_photo_label.image = student_photo
student_photo_label.pack(pady=(100, 20))

student_login_label = tk.Label(student_frame, text="STUDENT LOGIN", font=("Times New Roman", 19,"bold"), bg='lavender')
student_login_label.pack()

student_login_box = tk.Frame(student_frame,bg='lavender')
student_login_box.pack(padx=100, pady=(20, 100))

student_username_label = tk.Label(student_login_box, text="USERNAME:", font=("Times New Roman", 15),bg='lavender')
student_username_label.grid(row=0, column=0, sticky='w')

student_username_entry = tk.Entry(student_login_box, font=("Times New Roman", 15))
student_username_entry.grid(row=0, column=1, padx=(10, 0))

student_password_label = tk.Label(student_login_box, text="PASSWORD:", font=("Times New Roman", 15), bg='lavender')
student_password_label.grid(row=1, column=0, sticky='w')

student_password_entry = tk.Entry(student_login_box, font=("Times New Roman", 15), show='*')
student_password_entry.grid(row=1, column=1, padx=(10, 0), pady=(0, 10))

student_login_button = tk.Button(student_login_box, text="LOGIN", font=("Times New Roman", 15), bg='green', command=student_login)
student_login_button.grid(row=2, columnspan=2, pady=(0, 10))

root.geometry(f"{screen_width}x{screen_height}+0+0")  # Set window size to full screen

# Start the Tkinter event loop
root.mainloop()