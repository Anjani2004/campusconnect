> **CAMPUS CONNECT**

> **Project Description:**

        Campus Connect is an innovative platform developed for [Shri Vishnu Engineering college for
        Women],designed to seamlessly integrate students into the vibrant pulse of campus life. The project leverages
        the official college website, [svecw.ac.in], as a primary data source to provide real-time information
        about a myriad of campus events. This includes event names,descriptions, dates, and times,all presented
        through an intuitive web interface.This data is elegantly presented through an intuitive and visually captivating
        web interface, ensuring that students are always in the loop regarding the latest happenings on campus.
        
> **REQUIREMENTS**

**1. Development Environment:**

**- Dependencies**
List of software, libraries, or tools that this project depends on


Python: 3.6 or higher

Pillow: 10.2.0

pip: 24.0

tk 0.1.0

DB Browser for SQLite: 3.12.2


**- Installation**
To set up the project locally, follow these steps:


Navigate to the project directory:

cd project



**- Install the required dependencies**

python -m pip install -r requirements.txt


> **2. Git:**
    -Git is required for version control and repository management.
    -Install Git from git-scm.com.



**- Project Dependencies**
After setting up the development environment, navigate to the project directory and install the
required dependencies
    



**- Running the Application**
Ensure you have a running instance of the Campus Connect application:
    -npm start





**- CONFIGURATION**
Customize the application by updating configuration files as needed. Key configuration details
include:
Official College Website URL: [www.svecw.ac.in](https://svecw.ac.in/Default.aspx?ReturnUrl=%2fStudentMaster.aspx)
Update the URL in the configuration files to match the college's official website.


> **3. Getting Started:**
Now the project is set up, you can run it with the following command:

python main.py 
